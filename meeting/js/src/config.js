jQuery.noConflict();

(function($, PLUGIN_ID) {
  'use strict';
  $(function() {
    const message = {
      'en': {
        'msg_app_id': 'Tencent Meeting AppID',
        'msg_secret_id': 'Tencent Meeting Secret ID',
        'msg_secret_key': 'Tencent Meeting Secret Key',
        'msg_account': 'Tencent Meeting Account',
        'msg_account_description': 'Corporate users can use English names for corporate accounts, and individual users can use mobile phone numbers',
        'msg_meeting_subject': 'Tencent Meeting Subject',
        'msg_meeting_subject_description': 'Single-line text field(must be different from the "Tencent Meeting URL" and' +
        ' "Tencent Meeting ID" field)',
        'msg_meeting_id': 'Tencent Meeting ID',
        'msg_meeting_id_description': 'Single-line text field(must be different from the "Tencent Meeting URL" and' +
        ' "Tencent Meeting Subject" field)',
        'msg_meeting_URL': 'Tencent Meeting URL',
        'msg_meeting_URL_description': 'Single-line text field(must be different from the "Tencent Meeting ID" and' +
        ' "Tencent Meeting Subject" field)',
        'msg_meeting_startTime': 'Tencent Meeting Start Time',
        'msg_meeting_startTime_description': 'DateTime field(must be different from the "Tencent Meeting End Time")',
        'msg_meeting_endTime': 'Tencent Meeting End Time',
        'msg_meeting_endTime_description': 'DateTime field(must be different from the "Tencent Meeting Start Time")',
        'msg_plugin_submit': '     Save   ',
        'msg_plugin_cancel': '     Cancel   ',
        'msg_required_field': 'Please enter the required field.',
        'msg_text_fields_are_sames': 'The values of the "Tencent Meeting ID" and "Tencent Meeting URL" fields must be different.',
        'msg_time_fields_are_same': '“腾讯会议开始时间”和“腾讯会议结束时间”字段的值必须不同。'
      },
      'zh': {
        'msg_app_id': '腾讯会议AppID',
        'msg_secret_id': '腾讯会议身份识别ID',
        'msg_secret_key': '腾讯会议身份密钥',
        'msg_account': '腾讯会议账号',
        'msg_account_description': '企业用户可以为企业账户英文名、个人用户可以为手机号',
        'msg_meeting_subject': '腾讯会议主题',
        'msg_meeting_subject_description': '单行文本字段（必须与“腾讯会议链接”和“腾讯会议ID”字段不同）',
        'msg_meeting_id': '腾讯会议ID',
        'msg_meeting_id_description': '单行文本字段（必须与“腾讯会议链接”和“腾讯会议主题”字段不同）',
        'msg_meeting_URL': '腾讯会议链接',
        'msg_meeting_URL_description': '单行文本字段（必须与“腾讯会议ID”和“腾讯会议主题”字段不同）',
        'msg_meeting_startTime': '腾讯会议开始时间',
        'msg_meeting_startTime_description': '日期时间字段（必须与“腾讯会议结束时间”字段不同）',
        'msg_meeting_endTime': '腾讯会议结束时间',
        'msg_meeting_endTime_description': '日期时间字段（必须与“腾讯会议开始时间”字段不同）',
        'msg_plugin_submit': '     保存   ',
        'msg_plugin_cancel': '     返回   ',
        'msg_required_field': '请输入必填字段。',
        'msg_text_fields_are_same': '“腾讯会议主题”、“腾讯会议ID”和“腾讯会议链接”字段的值必须不同。',
        'msg_time_fields_are_same': '“腾讯会议开始时间”和“腾讯会议结束时间”字段的值必须不同。'
      }
    };

    const lang = kintone.getLoginUser().language;
    const i18n = (lang in message) ? message[lang] : message['zh'];

    $('span[datatype="translate"]').each(function(i, elt) {
      $(elt).text(i18n[$(elt).attr('data-content')]);
    });

    const config = kintone.plugin.app.getConfig(PLUGIN_ID);
    if (config['appId']) {
      $('#appId').val(config['appId']);
    }
    if (config['secretId']) {
      $('#secretId').val(config['secretId']);
    }
    if (config['secretKey']) {
      $('#secretKey').val(config['secretKey']);
    }
    if (config['account']) {
      $('#account').val(config['account']);
    }

    KintoneConfigHelper.getFields('SINGLE_LINE_TEXT').then(function(resp) {
      for (let i = 0; i < resp.length; i++) {
        if (resp[i].hasOwnProperty('subtableCode')) continue;
        $('#meetingSubject').append($('<OPTION>').text(resp[i]['label']).val(resp[i]['code']));
        $('#meetingId').append($('<OPTION>').text(resp[i]['label']).val(resp[i]['code']));
        $('#meetingUrl').append($('<OPTION>').text(resp[i]['label']).val(resp[i]['code']));
      }
      if (config['meetingSubject']) {
        $('#meetingSubject').val(config['meetingSubject']);
      }
      if (config['meetingId']) {
        $('#meetingId').val(config['meetingId']);
      }
      if (config['meetingUrl']) {
        $('#meetingUrl').val(config['meetingUrl']);
      }
    }).catch(function(err) {
      console.log(err);
    });

    KintoneConfigHelper.getFields('DATETIME').then(function(resp) {
      for (let i = 0; i < resp.length; i++) {
        if (resp[i].hasOwnProperty('subtableCode')) continue;
        $('#startTime').append($('<OPTION>').text(resp[i]['label']).val(resp[i]['code']));
        $('#endTime').append($('<OPTION>').text(resp[i]['label']).val(resp[i]['code']));
      }
      if (config['startTime']) {
        $('#startTime').val(config['startTime']);
      }
      if (config['endTime']) {
        $('#endTime').val(config['endTime']);
      }
    }).catch(function(err) {
      console.log(err);
    });

    $('#plugin_submit').click(function() {
      const appId = $('#appId').val().trim();
      const secretId = $('#secretId').val().trim();
      const secretKey = $('#secretKey').val().trim();
      const account = $('#account').val().trim();
      const meetingId = $('#meetingId').val().trim();
      const meetingUrl = $('#meetingUrl').val().trim();
      const meetingSubject = $('#meetingSubject').val().trim();
      const startTime = $('#startTime').val().trim();
      const endTime = $('#endTime').val().trim();

      if (appId === null || secretId === null || secretKey === null || account === null || meetingId === null ||
        meetingUrl === null || meetingSubject === null || startTime === null || endTime === null) {
        alert(i18n.msg_required_field);
        return;
      }
      if (appId.length === 0 || secretId.length === 0 || secretKey.length === 0 || account.length === 0 ||
        meetingId.length === 0 || meetingUrl.length === 0 || meetingSubject.length === 0 || startTime.length === 0 ||
        endTime.length === 0) {
        alert(i18n.msg_required_field);
        return;
      }
      if (meetingId === meetingUrl || meetingId === meetingSubject || meetingUrl === meetingSubject) {
        alert(i18n.msg_text_fields_are_same);
        return;
      }
      if (startTime === endTime) {
        alert(i18n.msg_time_fields_are_same);
        return;
      }
      const submitConfig = {};
      submitConfig['appId'] = appId;
      submitConfig['secretId'] = secretId;
      submitConfig['secretKey'] = secretKey;
      submitConfig['account'] = account;
      submitConfig['meetingSubject'] = meetingSubject;
      submitConfig['meetingId'] = meetingId;
      submitConfig['meetingUrl'] = meetingUrl;
      submitConfig['startTime'] = startTime;
      submitConfig['endTime'] = endTime;

      kintone.plugin.app.setConfig(submitConfig);
    });

    $('#plugin_cancel').click(function() {
      history.back();
    });

  });
})(jQuery, kintone.$PLUGIN_ID);